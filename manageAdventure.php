<?php
include './classes/init.php';

if (isset($_GET['id'])) {
    if (isset($_GET['ref']) && $_GET['ref'] == "create") {
        $createdText = "Aventura creada!";
    }

    $realmID = $db->escape_string($_GET['id']);
    $query = "SELECT * FROM Realm WHERE id = '" . $realmID . "'";
    $results = $db->query($query);
    if ($db->num_rows($results) == 0) {
        return; //Invalid realm ID
    }
    $row = $db->fetch_array($results);

    $name = $row['name'];
    $description = $row['description'];
}
if (isset($_POST['name'])) {
    $name = $db->escape_string($_POST['name']);
    $description = $db->escape_string($_POST['description']);
    $charsIDs = array();
    foreach ($_POST['chars'] as $char) {
        array_push($charsIDs, $db->escape_string($char));
    }
    $query = "UPDATE Realm SET name='" . $name . "', description='" . $description . "' WHERE id = '" . $realmID . "'";
    $db->query($query);

    //Change this for an invitation instead of forcing the char to be in this realm
    $query = "UPDATE Chars SET realmID='" . $realmID . "' WHERE id IN ('" . implode($charsIDs, "', '") . "')";
    $db->query($query);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Administración de Aventura</title>
</head>
<body>
<div>
    <h3 style="color: green;"><?= $createdText ?></h3>
</div>
<div>
    <h1>Administración de Aventura</h1>
    <form method="post" action="#">
        <label for="name">Nombre de la aventura </label>
        <div>
            <input id="name" name="name" type="text" maxlength="40" value="<?= $name ?>"/>
        </div>
        <label for="description">Descripción (opcional) </label>
        <div>
            <textarea id="description" name="description" type="text" cols="40" rows="5" maxlength="255"
                      value=""><?= $description ?></textarea>
        </div>

        <label for="chars">Elegir integrantes (Ctrl + click) </label>
        <div>
            <select id="chars" name="chars[]" multiple>
                <?php
                //Members
                $query = "SELECT * FROM Chars WHERE realmID = '" . $realmID . "'";
                $results = $db->query($query);
                $memberIDs = array();
                while ($row = $db->fetch_array($results)) {
                    echo "<option value='" . $row['id'] . "' selected>" . $row['name'] . "</option>";
                    array_push($memberIDs, $row['id']);
                }

                //Non-members
                if (empty($memberIDs)) {
                    $query = "SELECT * FROM Chars";
                } else {
                    $query = "SELECT * FROM Chars WHERE id NOT IN ('" . implode($memberIDs, "', '") . "')";
                }
                $results = $db->query($query);
                while ($row = $db->fetch_array($results)) {
                    echo "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
                }
                ?>
            </select>
        </div>

        <input id="saveForm" type="submit" name="submit" value="Guardar"/>
    </form>
</div>

<div>
    <h3 style="color: red;"><?= $errorText ?></h3>
</div>
</body>
</html>