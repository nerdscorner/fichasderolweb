<?php
include '../classes/init.php';
include '../classes/Realm.php';

if ($_GET['op'] == "fetchPlayerRealms") {
    $playerid = $db->escape_string($_GET['playerid']);
    $query = "SELECT R.id, R.name, R.description, C.id AS characterId, C.name AS characterName FROM Realm AS R JOIN Chars AS C ON (R.id = C.realmID)WHERE C.playerID = " . $playerid;
    $results = $db->query($query);
    $realms = array();
    while ($row = $db->fetch_array($results)) {
        array_push($realms, $row);
    }
    echo json_encode($realms);
} else if ($_GET['op'] == "fetchDmRealms") {
    $dmid = $db->escape_string($_GET['dmid']);
    $query = "SELECT R.id, R.name, R.description, count(C.id) AS setCharacterCount FROM Realm AS R JOIN DM AS D ON (R.dmId = D.id) LEFT JOIN Chars AS C ON (R.id = C.realmID) WHERE D.id = '" . $dmid . "' GROUP BY R.id";
    $results = $db->query($query);
    $realms = array();
    while ($row = $db->fetch_array($results)) {
        $realm = new Realm();
        $realm->setId($row['id']);
        $realm->setName($row['name']);
        $realm->setDescription($row['description']);
        $realm->setCharacterCount($row['setCharacterCount']);
        array_push($realms, $realm->toArray());
    }
    echo json_encode($realms);
} else if ($_GET['op'] == "fetchCharactersOnRealms") {
    $realmid = $db->escape_string($_GET['realmid']);
    $query = "SELECT * FROM Chars WHERE realmID = " . $realmid;
    $results = $db->query($query);
    $chars = array();
    while ($row = $db->fetch_array($results)) {
        array_push($chars, $row);
    }
    echo json_encode($chars);
}
