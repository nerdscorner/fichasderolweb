<?php

class Notifications {
    function __construct() {

    }

    private function sendNotification($data, $regIds) {
        $apiKey = "AIzaSyAXQlJD20pw8DI-uOoUBBv9lbQvGjWD3yU";
        $url = 'https://android.googleapis.com/gcm/send';
        $post = array('registration_ids' => $regIds, 'data' => $data);
        $headers = array('Authorization: key=' . $apiKey, 'Content-Type: application/json; charset=utf-8');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'GCM error: ' . curl_error($ch);
        }
        curl_close($ch);
    }

    public function sendBuffNotification($senderName, $regIds) {
        $data = array('type' => 'receiveBuff', 'senderName' => $senderName);
        $this->sendNotification($data, $regIds);
    }

    public function removeBuffNotification($senderName, $regIds) {
        $data = array('type' => 'removeBuff', 'senderName' => $senderName);
        $this->sendNotification($data, $regIds);
    }

    public function characterChangedNotification($senderName, $regIds) {
        $data = array('type' => 'characterChanged', 'senderName' => $senderName);
        $this->sendNotification($data, $regIds);
    }
}
