<?php
include '../classes/init.php';
include '../classes/User.php';

if ($_GET['op'] == "login") {
    $username = $db->escape_string($_GET['username']);
    $password = $db->escape_string($_GET['password']);
    $usertype = $db->escape_string($_GET['usertype']);
    $table = $usertype == "player" ? "Player" : "DM";
    $query = "SELECT * FROM " . $table . " WHERE UPPER(username) = UPPER('" . $username . "') AND UPPER(password) = UPPER('" . $password . "')";
    $results = $db->query($query);
    if ($db->num_rows($results) == 1) {
        $row = $db->fetch_array($results);

        $user = new User();
        $user->setId($row['id']);
        $user->setUsername($row['username']);
        echo json_encode($user->toArray());
    } else {
        echo $username . "-" . $password . "-" . $usertype . "-" . $usertype . "///" . $query;
    }
}
