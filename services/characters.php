<?php
include '../classes/init.php';
include '../classes/Character.php';
include '../classes/Buff.php';
include './notifications.php';

if ($_GET['op'] == "getCharacter") {
    //*******************************//
    //          GET CHARACTER
    //*******************************//
    $characterId = $db->escape_string($_GET['characterid']);
    $query = "SELECT * FROM Chars where id = '" . $characterId . "'";
    $results = $db->query($query);
    $row = $db->fetch_array($results);
    $character = new Character($row);

    $query = "SELECT DISTINCT buffId FROM Char_Buff where charId = '" . $characterId . "' AND type=0";
    $results = $db->query($query);
    $givenBuffIDs = array();
    while ($row = $db->fetch_array($results)) {
        array_push($givenBuffIDs, $row['buffId']);
    }
    if (!empty($givenBuffIDs)) {
        $query = "SELECT * FROM Buff WHERE id IN ('" . implode($givenBuffIDs, "', '") . "')";
        $results = $db->query($query);
        $givenBuffs = array();
        while ($row = $db->fetch_array($results)) { //All character buffs
            array_push($givenBuffs, $row);
        }
        $character->setGivenBuffs($givenBuffs);
    }

    $query = "SELECT buffId FROM Char_Buff where charId = '" . $characterId . "' AND type=1";
    $results = $db->query($query);
    $availableBuffIDs = array();
    while ($row = $db->fetch_array($results)) {
        array_push($availableBuffIDs, $row['buffId']);
    }
    if (!empty($availableBuffIDs)) {
        $query = "SELECT * FROM Buff WHERE id IN ('" . implode($availableBuffIDs, "', '") . "')";
        $results = $db->query($query);
        $availableBuffs = array();
        while ($row = $db->fetch_array($results)) { //All character buffs
            array_push($availableBuffs, $row);
        }
        $character->setAvailableBuffs($availableBuffs);
    }

    echo json_encode($character->toArray());
} else if ($_GET['op'] == "saveCharacter") {
    //*******************************//
    //          SAVE CHARACTER
    //*******************************//
    $characterjson = $_POST['characterjson'];
    $realmId = $db->escape_string($_GET['realmId']);
    $character = new Character(json_decode($characterjson));
    $id = $character->getId();
    $name = $db->escape_string($character->getName());
    $city = $db->escape_string($character->getCity());
    $age = $db->escape_string($character->getAge());
    $damageType = $db->escape_string($character->getDamageType());
    $hitPoints = $db->escape_string($character->getHitPoints());
    $maxHitPoints = $db->escape_string($character->getMaxHitPoints());
    $manaPoints = $db->escape_string($character->getManaPoints());
    $maxManaPoints = $db->escape_string($character->getMaxManaPoints());
    $experience = $db->escape_string($character->getExperience());
    $advantages = $db->escape_string($character->getAdvantages());
    $disadvantages = $db->escape_string($character->getDisadvantages());

    $gold = $db->escape_string($character->getGold());
    $silver = $db->escape_string($character->getSilver());
    $copper = $db->escape_string($character->getCopper());
    $knownMagic = $db->escape_string($character->getKnownMagic());
    $items = $db->escape_string($character->getItems());
    $history = $db->escape_string($character->getHistory());

    //STATS
    $str = $db->escape_string($character->getStr());
    $dex = $db->escape_string($character->getDex());
    $res = $db->escape_string($character->getRes());
    $arm = $db->escape_string($character->getArm());
    $wit = $db->escape_string($character->getWit());

    //TEMP STATS
    $strTmp = $db->escape_string($character->getStrTmp());
    $dexTmp = $db->escape_string($character->getDexTmp());
    $resTmp = $db->escape_string($character->getResTmp());
    $armTmp = $db->escape_string($character->getArmTmp());
    $witTmp = $db->escape_string($character->getWitTmp());

    $query = "UPDATE Chars SET name='" . $name . "',  city='" . $city . "',  age=" . $age . ",  damageType='" . $damageType .
        "',  hitPoints=" . $hitPoints . ",  maxHitPoints=" . $maxHitPoints . ",  manaPoints=" . $manaPoints . "," .
        "maxManaPoints=" . $maxManaPoints . ",  experience=" . $experience . ",  advantages='" . $advantages . "',  disadvantages='" . $disadvantages .
        "',  str=" . $str . ",  strTmp=" . $strTmp .",  dex=" . $dex . ",  dexTmp=" . $dexTmp .
        ", res=" . $res . ", resTmp=" . $resTmp . ", arm=" . $arm . ", armTmp=" . $armTmp . ", wit=" . $wit .", witTmp=" . $witTmp .
        ", gold=" . $gold . ", silver=" . $silver . ", copper=" . $copper .
        ", knownMagic='" . $knownMagic . "'," . "items='" . $items . "', history='" . $history .
        "' WHERE id = '" . $id . "'";
    $results = $db->query($query);

    //SEND NOTIFICATIONS TO DM
    $query = "SELECT D.regId FROM DM AS D JOIN Realm AS R ON (D.id = R.dmId) WHERE R.id = '" . $realmId . "'";
    $results = $db->query($query);
    $row = $db->fetch_array($results);
    $regIds = array();
    array_push($regIds, $row['regId']);

    $query = "SELECT regId FROM Chars WHERE id = '" . $id . "'";
    $results = $db->query($query);
    $row = $db->fetch_array($results);
    array_push($regIds, $row['regId']);
    $notifications = new Notifications();
    $notifications->characterChangedNotification($name, $regIds);
} else if ($_GET['op'] == "setBuff") {
    //*******************************//
    //          SET BUFFS
    //*******************************//
    $mycharacterId = $db->escape_string($_POST['mycharacterId']);
    $mycharacterName = $db->escape_string($_POST['mycharacterName']);
    $charactersids = $db->escape_string($_POST['charactersids']);
    $buffsIds = $db->escape_string($_POST['buffsIds']);
    $realmId = $db->escape_string($_POST['realmId']);

    if ($charactersids != "" && $buffsIds != "") {
        //At least one character and one buff has been selected
        $charIdsArray = explode(",", $charactersids);
        $buffsIdsArray = explode(",", $buffsIds);
        $values = array();
        for ($i = 0; $i < count($buffsIdsArray); $i++) {
            $buffId = $buffsIdsArray[$i];
            for ($j = 0; $j < count($charIdsArray); $j++) {
                $charId = $charIdsArray[$j];
                $valueRow = "( '" . $charId . "' , '" . $buffId . "' , '0', '" . $mycharacterId . "')";
                array_push($values, $valueRow);
            }
        }
        //INSERT NEW BUFFS
        $query = "INSERT INTO Char_Buff (charId, buffId, type, giver) VALUES " . implode($values, ", ");
        $db->query($query);

        $query = "SELECT sum(manaCost) as manaCost FROM Buff WHERE id IN ('" . implode($buffsIdsArray, "', '") . "')";
        $results = $db->query($query);
        $row = $db->fetch_array($results);
        $manaCost = $row['manaCost'] * count($charIdsArray);

        $query = "UPDATE Chars SET manaPoints=manaPoints-" . $manaCost . " WHERE id='" . $mycharacterId . "'";
        $db->query($query);

        //SEND NOTIFICATIONS TO BUFFS RECEIVERS
        $query = "SELECT regId FROM Chars WHERE id IN ('" . implode($charIdsArray, "', '") . "')";
        $results = $db->query($query);
        $regIds = array();
        while ($row = $db->fetch_array($results)) {
            array_push($regIds, $row['regId']);
        }
        $notifications = new Notifications();
        $notifications->sendBuffNotification($mycharacterName, $regIds);

        //SEND NOTIFICATIONS TO DM
        $query = "SELECT D.regId FROM DM AS D JOIN Realm AS R ON (D.id = R.dmId) WHERE R.id = '" . $realmId . "'";
        $results = $db->query($query);
        $row = $db->fetch_array($results);
        $regIds = array();
        array_push($regIds, $row['regId']);
        $notifications->characterChangedNotification($mycharacterName, $regIds);
    }
} else if ($_GET['op'] == "clearBuffs") {
    //*******************************//
    //          CLEAR BUFFS
    //*******************************//
    $mycharacterId = $db->escape_string($_GET['mycharacterId']);
    $mycharacterName = $db->escape_string($_GET['mycharacterName']);
    $realmId = $db->escape_string($_GET['realmId']);
    //GET REMOVED CHARACTERS BUFF REGID
    $query = "SELECT DISTINCT charId FROM Char_Buff WHERE giver='" . $mycharacterId . "' AND type='0'";
    $results = $db->query($query);
    $charIds = array();
    while ($row = $db->fetch_array($results)) {
        array_push($charIds, $row['charId']);
    }

    //REMOVE BUFFS
    $query = "DELETE FROM Char_Buff WHERE giver='" . $mycharacterId . "' AND type='0'";
    $db->query($query);

    //SEND NOTIFICATIONS
    $query = "SELECT regId FROM Chars WHERE id IN ('" . implode($charIds, "', '") . "')";
    $results = $db->query($query);
    $regIds = array();
    while ($row = $db->fetch_array($results)) {
        array_push($regIds, $row['regId']);
    }
    $notifications = new Notifications();
    $notifications->removeBuffNotification($mycharacterName, $regIds);

    //SEND NOTIFICATIONS TO DM
    $query = "SELECT D.regId FROM DM AS D JOIN Realm AS R ON (D.id = R.dmId) WHERE R.id = '" . $realmId . "'";
    $results = $db->query($query);
    $row = $db->fetch_array($results);
    $regIds = array();
    array_push($regIds, $row['regId']);
    $notifications->characterChangedNotification($mycharacterName, $regIds);
} else if ($_GET['op'] == "getCharacters") {
    //*******************************//
    //          GET REALM CHARACTERS
    //*******************************//
    $realmId = $db->escape_string($_GET['realmId']);
    $query = "SELECT id, name FROM Chars where realmId = '" . $realmId . "'";
    $results = $db->query($query);
    $characters = array();
    while ($row = $db->fetch_array($results)) {
        array_push($characters, $row);
    }
    echo json_encode($characters);
} else if ($_GET['op'] == "registerGCMRegId") {
    //*******************************//
    //         REGISTER FOR GCM
    //*******************************//
    $regid = $db->escape_string($_GET['regid']);
    $characterid = $db->escape_string($_GET['characterid']);
    $usertype = $db->escape_string($_GET['usertype']);
    $table = $usertype == "player" ? "Chars" : "DM";
    $query = "UPDATE " . $table . " SET regid='" . $regid . "' WHERE id = '" . $characterid . "'";
    $db->query($query);
} else if ($_GET['op'] == "unregisterGCMRegId") {
    //*******************************//
    //          UNREGISTER FOR GCM
    //*******************************//
    $characterid = $db->escape_string($_GET['characterid']);
    $usertype = $db->escape_string($_GET['usertype']);
    $table = $usertype == "player" ? "Chars" : "DM";
    $query = "UPDATE " . $table . " SET regid='' WHERE id = '" . $characterid . "'";
    $db->query($query);
}
