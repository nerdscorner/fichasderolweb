<?php

class Character {
    private $id;
    private $imageUrl;
    private $name;
    private $city;
    private $age;
    private $damageType;
    private $attackForce;
    private $hitPoints;
    private $maxHitPoints;
    private $manaPoints;
    private $maxManaPoints;
    private $experience;
    private $advantages;
    private $disadvantages;
    private $str;
    private $strTmp;
    private $dex;
    private $dexTmp;
    private $res;
    private $resTmp;
    private $arm;
    private $armTmp;
    private $wit;
    private $witTmp;

    private $gold;
    private $silver;
    private $copper;
    private $knownMagic;
    private $items;
    private $history;

    private $givenBuffs;
    private $availableBuffs;
    private $isWizard;

    function __construct($data) {
        foreach ($data as $key => $val) {
            if (property_exists(__CLASS__, $key)) {
                $this->$key = $val;
            }
        }
    }

    public function setId($s) {
        $this->id = $s;
    }

    public function setName($s) {
        $this->name = $s;
    }

    public function setCity($s) {
        $this->city = $s;
    }

    public function setAge($s) {
        $this->age = $s;
    }

    public function setDamageType($s) {
        $this->damageType = $s;
    }

    public function setAttackForce($s) {
        $this->attackForce = $s;
    }

    public function setHitPoints($s) {
        $this->hitPoints = $s;
    }

    public function setMaxHitPoints($s) {
        $this->maxHitPoints = $s;
    }

    public function setManaPoints($s) {
        $this->manaPoints = $s;
    }

    public function setMaxManaPoints($s) {
        $this->maxManaPoints = $s;
    }

    public function setExperience($s) {
        $this->experience = $s;
    }

    public function setAdvantages($s) {
        $this->advantages = $s;
    }

    public function setDisadvantages($s) {
        $this->disadvantages = $s;
    }

    //STATS
    public function setStr($s) {
        $this->str = $s;
    }

    public function setStrTmp($s) {
        $this->strTmp = $s;
    }

    public function setDex($s) {
        $this->dex = $s;
    }

    public function setDexTmp($s) {
        $this->dexTmp = $s;
    }

    public function setRes($s) {
        $this->res = $s;
    }

    public function setResTmp($s) {
        $this->resTmp = $s;
    }

    public function setArm($s) {
        $this->arm = $s;
    }

    public function setArmTmp($s) {
        $this->armTmp = $s;
    }

    public function setWit($s) {
        $this->wit = $s;
    }

    public function setWitTmp($s) {
        $this->witTmp = $s;
    }

    public function setGivenBuffs($s) {
        $this->givenBuffs = $s;
    }

    public function setAvailableBuffs($s) {
        $this->availableBuffs = $s;
    }


    //BACK PAGE

    public function setGold($s) {
        $this->gold = $s;
    }

    public function setSilver($s) {
        $this->silver = $s;
    }

    public function setCopper($s) {
        $this->copper = $s;
    }

    public function setKnownMagic($s) {
        $this->knownMagic = $s;
    }

    public function setItems($s) {
        $this->items = $s;
    }

    public function setHistory($s) {
        $this->history = $s;
    }

    public function setImageUrl($s) {
        $this->imageUrl = $s;
    }

    public function setIsWizard($s) {
        $this->isWizard = $s;
    }


    //GETTERS
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getCity() {
        return $this->city;
    }

    public function getAge() {
        return $this->age;
    }

    public function getDamageType() {
        return $this->damageType;
    }

    public function getAttackForce() {
        return $this->attackForce;
    }

    public function getHitPoints() {
        return $this->hitPoints;
    }

    public function getMaxHitPoints() {
        return $this->maxHitPoints;
    }

    public function getManaPoints() {
        return $this->manaPoints;
    }

    public function getMaxManaPoints() {
        return $this->maxManaPoints;
    }

    public function getExperience() {
        return $this->experience;
    }

    public function getAdvantages() {
        return $this->advantages;
    }

    public function getDisadvantages() {
        return $this->disadvantages;
    }

    public function getIsWizard() {
        return $this->isWizard;
    }

    //STATS
    public function getStr() {
        return $this->str;
    }

    public function getStrTmp() {
        return $this->strTmp;
    }

    public function getDex() {
        return $this->dex;
    }

    public function getDexTmp() {
        return $this->dexTmp;
    }

    public function getRes() {
        return $this->res;
    }

    public function getResTmp() {
        return $this->resTmp;
    }

    public function getArm() {
        return $this->arm;
    }

    public function getArmTmp() {
        return $this->armTmp;
    }

    public function getWit() {
        return $this->wit;
    }

    public function getWitTmp() {
        return $this->witTmp;
    }

    //BACK PAGE
    public function getGold() {
        return $this->gold;
    }

    public function getSilver() {
        return $this->silver;
    }

    public function getCopper() {
        return $this->copper;
    }

    public function getKnownMagic() {
        return $this->knownMagic;
    }

    public function getItems() {
        return $this->items;
    }

    public function getHistory() {
        return $this->history;
    }

    public function getImageUrl() {
        return $this->imageUrl;
    }

    public function getGivenBuffs() {
        return $this->givenBuffs;
    }

    public function getAvailableBuffs() {
        return $this->availableBuffs;
    }

    public function toArray() {
        $ret = array();
        $ret['id'] = $this->id;
        $ret['imageUrl'] = $this->imageUrl;
        $ret['name'] = $this->name;
        $ret['city'] = $this->city;
        $ret['age'] = $this->age;
        $ret['damageType'] = $this->damageType;
        $ret['attackForce'] = $this->attackForce;
        $ret['hitPoints'] = $this->hitPoints;
        $ret['maxHitPoints'] = $this->maxHitPoints;
        $ret['manaPoints'] = $this->manaPoints;
        $ret['maxManaPoints'] = $this->maxManaPoints;
        $ret['experience'] = $this->experience;
        $ret['advantages'] = $this->advantages;
        $ret['disadvantages'] = $this->disadvantages;
        $ret['isWizard'] = $this->isWizard;

        $ret['gold'] = $this->gold;
        $ret['silver'] = $this->silver;
        $ret['copper'] = $this->copper;
        $ret['knownMagic'] = $this->knownMagic;
        $ret['items'] = $this->items;
        $ret['history'] = $this->history;

        $ret['givenBuffs'] = $this->givenBuffs;
        $ret['availableBuffs'] = $this->availableBuffs;

        //STATS
        $ret['str'] = $this->str;
        $ret['strTmp'] = $this->strTmp;
        $ret['dex'] = $this->dex;
        $ret['dexTmp'] = $this->dexTmp;
        $ret['res'] = $this->res;
        $ret['resTmp'] = $this->resTmp;
        $ret['arm'] = $this->arm;
        $ret['armTmp'] = $this->armTmp;
        $ret['wit'] = $this->wit;
        $ret['witTmp'] = $this->witTmp;
        return $ret;
    }
}
