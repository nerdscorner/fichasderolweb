<?php

class Realm {
    private $id;
    private $name;
    private $description;
    private $characterId;
    private $characterName;
    private $characterCount;

    function __construct() {

    }

    public function setName($s) {
        $this->name = $s;
    }

    public function setId($s) {
        $this->id = $s;
    }

    public function setDescription($s) {
        $this->description = $s;
    }

    public function setCharacterName($s) {
        $this->characterName = $s;
    }

    public function setCharacterId($s) {
        $this->characterId = $s;
    }

    public function setCharacterCount($s) {
        $this->characterCount = $s;
    }

    public function toArray() {
        $ret = array();
        $ret['id'] = $this->id;
        $ret['name'] = $this->name;
        $ret['description'] = $this->description;
        $ret['characterId'] = $this->characterId;
        $ret['characterName'] = $this->characterName;
        $ret['characterCount'] = $this->characterCount;
        return $ret;
    }
}
