<?php

class Buff {
    private $id;
    private $name;
    private $description;
    private $str;
    private $dex;
    private $res;
    private $arm;
    private $wit;
    private $attackForce;
    private $manaCost;

    function __construct() {

    }

    public function setName($s) {
        $this->name = $s;
    }

    public function setId($s) {
        $this->id = $s;
    }

    public function setDescription($s) {
        $this->description = $s;
    }

    public function setStr($s) {
        $this->str = $s;
    }

    public function setDex($s) {
        $this->dex = $s;
    }

    public function setRes($s) {
        $this->res = $s;
    }

    public function setArm($s) {
        $this->arm = $s;
    }

    public function setWit($s) {
        $this->wit = $s;
    }

    public function setAttackForce($s) {
        $this->attackForce = $s;
    }

    public function setManaCost($s) {
        $this->manaCost = $s;
    }

    public function toArray() {
        $ret = array();
        $ret['id'] = $this->id;
        $ret['name'] = $this->name;
        $ret['description'] = $this->description;
        $ret['str'] = $this->str;
        $ret['dex'] = $this->dex;
        $ret['res'] = $this->res;
        $ret['arm'] = $this->arm;
        $ret['wit'] = $this->wit;
        $ret['attackForce'] = $this->attackForce;
        $ret['manaCost'] = $this->manaCost;
        return $ret;
    }
}
