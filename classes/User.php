<?php

class User {
    private $id;
    private $username;

    function __construct() {

    }

    public function setId($s) {
        $this->id = $s;
    }

    public function setUsername($s) {
        $this->username = $s;
    }

    public function toArray() {
        $ret = array();
        $ret['id'] = $this->id;
        $ret['username'] = $this->username;
        return $ret;
    }
}
