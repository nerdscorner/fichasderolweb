<?php
include './classes/init.php';

if (isset($_POST['name'])) {
    $name = $db->escape_string($_POST['name']);
    $description = $db->escape_string($_POST['description']);
    if (strlen(trim($name)) == 0) {
        $errorText = "Nombre vacío";
    } else {
        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];
        if (strcmp($password1, $password2) != 0) {
            $errorText = "Las contraseñas no coinciden";
        } else {
            $password1 = $db->escape_string($_POST['password1']);
            $password2 = $db->escape_string($_POST['password2']);
            $query = "INSERT INTO Realm " .
                "(id, name, description, password) " .
                "VALUES (NULL, '" . $name . "', '" . $description . "', '" . $password1 . "')";
            $db->query($query);
            $adventureId = $db->insert_id();
            header('Location: manageAdventure.php?ref=create&id=' . $adventureId);
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Crear Aventura</title>
</head>
<body>
<div>
    <h1>Crear Aventura</h1>
    <form method="post" action="#">
        <div>
            <p>Completa el siguiente formulario para crear tu aventura.</p>
        </div>
        <label for="name">Nombre de la aventura </label>
        <div>
            <input id="name" name="name" type="text" maxlength="40" value="<?= $name ?>"/>
        </div>
        <label for="password1">Contraseña</label>
        <div>
            <input id="password1" name="password1" type="password" maxlength="255" value=""/>
        </div>
        <label for="password2">Repetir contraseña</label>
        <div>
            <input id="password2" name="password2" type="password" maxlength="255" value=""/>
        </div>
        <label for="description">Descripción (opcional) </label>
        <div>
            <textarea id="description" name="description" type="text" cols="40" rows="5" maxlength="255"
                      value=""><?= $description ?></textarea>
        </div>
        <input id="saveForm" type="submit" name="submit" value="Crear"/>
    </form>
</div>

<div>
    <h3 style="color: red;"><?= $errorText ?></h3>
</div>
</body>
</html>