<?php
include './classes/init.php';

if (isset($_POST['name'])) {
    $name = $db->escape_string($_POST['name']);
    $maxHitPoints = $_POST['maxHitPoints'];
    $maxManaPoints = $_POST['maxManaPoints'];
    $city = $db->escape_string($_POST['city']);
    $age = $db->escape_string($_POST['age']);
    $damageType = $db->escape_string($_POST['damageType']);
    $advantages = $db->escape_string($_POST['advantages']);
    $disadvantages = $db->escape_string($_POST['disadvantages']);
    if (strlen(trim($name)) == 0) {
        $errorText = "Nombre vacío";
    } else {
        if (strlen(trim($maxHitPoints)) == 0) {
            $errorText = "Puntos de vida vacíos";
        } else {
            if (strlen(trim($maxManaPoints)) == 0) {
                $errorText = "Puntos de mana vacíos";
            } else {
                $query = "INSERT INTO Chars " .
                    "(		 id, 	name, 		city, 		  age, 		   damageType, 		hitPoints, 				maxHitPoints, 		manaPoints, 		maxManaPoints, 			advantages, 		disadvantages) " .
                    "VALUES (NULL, '" . $name . "', '" . $city . "', '" . $age . "', '" . $damageType . "', '" . $maxHitPoints . "', '" . $maxHitPoints . "', '" . $maxManaPoints . "', '" . $maxManaPoints . "', '" . $advantages . "', '" . $disadvantages . "')";
                $db->query($query);
            }
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Crear personaje</title>
</head>
<body>
<div>
    <h1>Crear personaje</h1>
    <form method="post" action="#">
        <div>
            <p>Completa el siguiente formulario para crear tu personaje.</p>
        </div>

        <label for="name">Nombre del personaje </label>
        <div>
            <input id="name" name="name" type="text" maxlength="40" value="<?= $name ?>"/>
        </div>

        <label for="city">Ciudad </label>
        <div>
            <input id="city" name="city" type="text" maxlength="40" value="<?= $city ?>"/>
        </div>

        <label for="age">Edad </label>
        <div>
            <input id="age" name="age" type="text" maxlength="40" value="<?= $age ?>"/>
        </div>

        <label for="damageType">Tipo de daño </label>
        <div>
            <input id="damageType" name="damageType" type="text" maxlength="40" value="<?= $damageType ?>"/>
        </div>

        <label for="maxHitPoints">Puntos de vida</label>
        <div>
            <input id="maxHitPoints" name="maxHitPoints" type="text" maxlength="40" value="<?= $maxHitPoints ?>"/>
        </div>

        <label for="maxManaPoints">Puntos de mana </label>
        <div>
            <input id="maxManaPoints" name="maxManaPoints" type="text" maxlength="40" value="<?= $maxManaPoints ?>"/>
        </div>

        <label for="advantages">Ventajas</label>
        <div>
            <textarea id="advantages" name="advantages" type="text" cols="40" rows="5" maxlength="255"
                      value="<?= $advantages ?>"></textarea>
        </div>

        <label for="disadvantages">Desventajas </label>
        <div>
            <textarea id="disadvantages" name="disadvantages" type="text" cols="40" rows="5" maxlength="255"
                      value="<?= $disadvantages ?>"></textarea>
        </div>

        <input id="saveForm" type="submit" name="submit" value="Crear"/>
    </form>
</div>

<div>
    <h3 style="color: red;"><?= $errorText ?></h3>
</div>
</body>
</html>