<?php
include './classes/init.php';

if (isset($_POST['username'])) {
    $username = $db->escape_string($_POST['username']);
    $password1 = $db->escape_string($_POST['password1']);
    $password2 = $db->escape_string($_POST['password2']);
    $usertype = $db->escape_string($_POST['usertype']);
    if (strlen(trim($username)) == 0) {
        $errorText = "Nombre de usuario vacío";
    } else {
        if (strcmp($password1, $password2) != 0) {
            $errorText = "Las contraseñas no coinciden";
        } else {
            $table = $usertype == "player" ? "Player" : "DM";
            $query = "INSERT INTO " . $table . " " .
                "(		 id, 	username, 		password) " .
                "VALUES (NULL, '" . $username . "', '" . sha1($password1) . "')";
            $db->query($query);
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Crear usuario</title>
</head>
<body>
<div>
    <h1>Crear usuario</h1>
    <form method="post" action="#">
        <div>
            <p>Completa el siguiente formulario para crear tu usuario.</p>
        </div>

        <label for="username">Nombre del usuario </label>
        <div>
            <input id="username" name="username" type="text" maxlength="40" value="<?= $username ?>"/>
        </div>

        <label for="password1">Contraseña </label>
        <div>
            <input id="password1" name="password1" type="password" maxlength="40" value=""/>
        </div>

        <label for="password2">Repetir contraseña </label>
        <div>
            <input id="password2" name="password2" type="password" maxlength="40" value=""/>
        </div>

        <label for="usertype">Tipo de usuario </label>
        <div>
            <select id="usertype" name="usertype">
                <option value="player">Jugador</option>
                <option value="dm">DM</option>
            </select>
        </div>

        <input id="saveForm" type="submit" name="submit" value="Crear"/>
    </form>
</div>

<div>
    <h3 style="color: red;"><?= $errorText ?></h3>
</div>
</body>
</html>